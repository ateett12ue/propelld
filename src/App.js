import React from "react";
import "./App.css";
import HomePage from "./Pages/HomePage";
import Navbar from "./Components/Navbar";
import Footer from "./Components/Footer";
function App() {
  return (
    <div>
      <Navbar />
      <HomePage />

      <Footer />
    </div>
  );
}

export default App;
